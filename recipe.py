def kancolle(recype) :
    if(recype == '구축함') :
        dt = ('```\n'
            '첫번째 레시피는 30 30 30 30\n'
        '두번째 레시피는 250 30 200 30(주로 경순, 중순 + 레어구축함을 노리는 거야!\n'
        '세번째 레시피는 250 130 200 30\n'
        '네번째 레시피는 270 130 330 30\n'
        '```')
        return dt
    elif(recype == '경순양함') :
        cl = ('```\n'
        '첫번째 레시피는 30 30 30 30\n'
        '두번째 레시피는 250 30 200 30(주로 경순, 중순 + 레어구축함 및 잠수함을 노릴거야!\n'
        '```')
        return cl
    elif(recype == '중순양함') :
        ca = ('```\n'
              '첫번째 레시피는 250 30 200 30\n'
        '두번째 레시피는 270 130 330 30\n'
        '세번째 레시피는 400 30 600 30\n'
        '```')
        return ca
    elif(recype == '전함') :
        cv = ('```\n'
              '첫번째 레시피는 400 30 600 30\n'
        '두번째 레시피는 400 100 600 30\n'
        '세번째 레시피는 400 30 700 30\n'
        '```')
        return cv
    elif(recype == '항공모함') :
        at = ('```\n'
              '첫번째 레시피는 300 30 400 300\n'
              '두번째 레시피는 350 30 400 350\n'
              '세번째 레시피는 300 30 600 400(레어급인 경항모들이 읍읍)\n'
              '네번째 레시피는 400 200 500 700\n'
              '다섯번째 레시피는 300 300 600 600\n'
              '마지막 레시피는 400 200 500 400\n'
              '```')
        return at
    elif(recype == '잠수함') :
        sm = ('```\n'
            '첫번째 레시피는 250 30 200 30\n'
              '두번째 레시피는 250 130 200 30\n'
              '세번째 레시피는 300 100 200 30\n'
              '네번째 레시피는 270 130 330 30\n'
              '```')
        return sm

def girlsfront(recype) :
    if(recype == 'HG') :
        HG = ('```\n'
              '첫번째 레시피는 130 130 130 130\n'
        '두번째 레시피는 130 130 130 230\n'
        '```')
        return HG
    elif(recype == 'SMG') :
        SMG = ('```\n'
               '첫번째 레시피는 430 430 30 230\n'
               '두번째 레시피는 430 430 130 230\n'
               '```')
        return SMG
    elif(recype == 'AR') :
        AR = ('```\n'
              '첫번째 레시피는 30 430 430 230\n'
              '두번째 레시피는 130 430 430 230\n'
              '세번째 레시피는 95 400 400 95\n'
              '```')
        return AR
    elif(recype == 'SPAR') :
        SPAR = ('```\n'
                '첫번째 레시피는 30 30 999 30\n'
                '두번째 레시피는 999 30 30 30\n'
                '```')
        return SPAR
    elif(recype == 'RF') :
        RF = ('```\n'
              '첫번째 레시피는 430 30 430 230\n'
              '두번째 레시피는 430 130 430 230\n'
              '```')
        return RF
    elif(recype == 'MG') :
        MG = ('```\n'
              '레시피는 600 600 100 400\n'
              '```')
        return MG
    elif(recype == '네게브') :
        Negave = ('```\n'
                  '레시피는 730 630 130 430\n'
                  '```')
        return Negave
    elif(recype == '범용') :
        UNI = ('```\n'
               '첫번째 레시피는 430 430 430 230\n'
               '두번째 레시피는 630 630 430 430\n'
               '```')
        return UNI
    elif(recype == '검열해제') :
        CODE = ('```\n'
                '레시피는 666 666 666 666\n'
                '```')
        return CODE
    elif(recype == 'SG') :
        SG = ('```\n'
              '첫번째 레시피는 6000 2000 6000 4000\n'
              '두번째 레시피는 6000 1000 6000 4000\n'
              '```')
        return SG
    elif(recype == '중형범용') :
        MUNI = ('```\n'
                '레시피는 6000 6000 6000 4000\n'
                '```')
        return MUNI

def girlsmake(time) :
    if(time == 20):
        return '2성 MK1911(HG), 나강 리볼버(HG), P38(HG)이 건조될 예정이야!'
    elif(time == 22):
        return '2성 PPK(HG)가 건조될 예정이야!'
    elif(time == 25):
        return '2성 FNP-9(HG), MP-446(HG)이 건조될 예정이야!'
    elif(time == 28) :
        return '2성 USP Compact(HG), Bren Ten(HG)이 건조될 예정이야!'
    elif(time == 30) :
        return '3성 P08(HG), C96(HG)이 건조될 예정이야!'
    elif(time == 35) :
        return '3성 92식(HG), P99(HG)가 건조될 예정이야!'
    elif(time == 40) :
        return '3성 아스트라 리볼버(HG), M9(HG), 마카로프(HG)가 건조될 예정이야!'
    elif(time == 45) :
        return '3성 토카레프(HG)가 건조될 예정이야!'
    elif(time == 50) :
        return '4성 콜트 리볼버(HG), Mk23(HG)이 건조될 예정이야!'
    elif(time == 52) :
        return '4성 스핏파이어(HG)가 건조될 예정이야!'
    elif(time == 55) :
        return '4성 P7(HG), 스테츠킨(HG)가 건조될 예정이야!'
    elif(time == 100) :
        return '5성 웰로드 MK2(HG)가 건조될 예정이야!'
    elif(time == 102) :
        return '5성 컨텐더(HG)가 건조될 예정이야!'
    elif(time == 105) :
        return '5성 M950A(HG), NZ75(HG)가 건조될 예정이야!'
    elif(time == 110) :
        return '5성 그리즐리 MK5(HG)와 2성 IDW(SMG), PP-2000(SMG)가 건조될 예정이야!'
    elif(time == 120) :
        return '2성 Spectre M4(SMG), M45(SMG)가 건조될 예정이야!'
    elif(time == 125) :
        return '2성 64식(SMG)이 건조될 예정이야!'
    elif(time == 130) :
        return '2성 MP40(SMG), 바레타 38형(SMG), M3(SMG)가 건조될 예정이야!'
    elif(time == 140) :
        return '3성 스텐 MK.2(SMG), 마이크로 우지(SMG)가 건조될 예정이야!'
    elif(time == 150) :
        return '2성 PPSh-41(SMG)가 건조될 예정이야!'
    elif(time == 200) :
        return '3성 MAC-10(SMG), 스콜피온(SMG)이 건조될 예정이야!'
    elif(time == 205) :
        return '3성 Z-62(SMG + 중제조)가 건조될 예정이야!'
    elif(time == 210) :
        return '3성 PPS-43(SMG)가 건조될 예정이야!'
    elif(time == 215) :
        return '4성 UMP9(SMG), UMP45(SMG)가 건조될 예정이야!'
    elif(time == 218) :
        return '4성 시프카(SMG)가 건조될 예정이야!'
    elif(time == 220) :
        return '4성 MP5(SMG), PP-90(SMG)가 건조될 예정이야!'
    elif(time == 225) :
        return '5성 수오미(SMG)가 건조될 예정이야!'
    elif(time == 230) :
        return '5성 톰슨(SMG), G36C(SMG)가 건조될 예정이야!'
    elif(time == 233) :
        return '5성 SR-3MP(SMG)가 건조될 예정이야!'
    elif(time == 235) :
        return '5성 벡터(SMG), 79식(SMG)가 건조될 예정이야!'
    elif(time == 240) :
        return '2성 갈릴(AR), SIG-510(AR)가 건조될 예정이야!'
    elif(time == 245) :
        return '2성 F2000(AR), 63식(AR)이 건조될 예정이야!'
    elif(time == 250) :
        return '2성 L85A1(AR), G3(AR)이 건조될 예정이야!'
    elif(time == 300) :
        return '3성 StG44(AR)이 건조될 예정이야!'
    elif(time == 310) :
        return '3성 OTs-12(AR), 2성 G43(RF), FN-49(RF)가 건조될 예정이야!'
    elif(time == 315) :
        return '3성 ARX-160(AR + 중제조)이 건조될 에정이야!'
    elif(time == 320) :
        return '3성 AK-47(AR), FNC(AR), 2성 BM59(RF)가 건조될 예정이야!'
    elif(time == 325) :
        return '4성 56-1식(AR)이 건조될 예정이야!'
    elif(time == 330) :
        return '4성 As Val(AR), FAMAS(AR), TAR-21(AR)과 2성 시모노프(RF), SVT-38(RF)이 건조될 예정이야!'
    elif(time == 335) :
        return '4성 9A-91(AR)이 건조될 예정이야!'
    elif(time == 340) :
        return '4성 G36(AR), 리베롤(AR)과 3성 M14(RF), SV-98(RF)가 건조될 예정이야!'
    elif(time == 345) :
        return '5성 FAL(AR)이 건조될 예정이야!'
    elif(time == 348) :
        return '5성 T91(AR)이 건조될 예정이야!'
    elif(time == 350) :
        return '5성 95식(AR), 97식(AR)과 3성 한양조88식(RF), OTs-44(RF + 중제조)가 건조될 예정이야!'
    elif(time == 355) :
        return '5성 HK416(AR)이 건조될 예정이야!'
    elif(time == 358) :
        return '5성 RFB(AR)이 건조될 예정이야!'
    elif(time == 400) :
        return '3성 M1 개런드(RF)가 건조될 예정이야!'
    elif(time == 404) :
        return '5성 G11(AR)이 건조될 예정이야!'
    elif(time == 405) :
        return '5성 G41(AR)이 건조될 예정이야!'
    elif(time == 410) :
        return '4성 모신나강(RF), T-5000(RF)가 건조될 예정이야!'
    elif(time == 415) :
        return '4성 SVD(RF)가 건조될 예정이야!'
    elif(time == 420) :
        return '4성 PSG-1(RF + 중제조), G28(RF + 중제조)이 건조될 예정이야!'
    elif(time == 425) :
        return '4성 스프링필드(RF)가 건조될 예정이야!'
    elif(time == 430) :
        return '4성 PTRD(RF)가 건조될 예정이야!'
    elif(time == 440) :
        return '5성 Kar98k(RF)가 건조될 예정이야!'
    elif(time == 445) :
        return '5성 NTW-20(RF)가 건조될 예정이야!'
    elif(time == 450) :
        return '5성 WA2000(RF)과 2성 AAT-52(MG), FG42(MG)가 건조될 예정이야!'
    elif(time == 452) :
        return '5성 IWS 2000(RF)이 건조될 예정이야!'
    elif(time == 455) :
        return '5성 M99(RF)가 건조될 예정이야!'
    elif(time == 500) :
        return '5성 리엔필드(RF)과 2성 MG34(MG), DP28(MG)가 건조될 예정이야!'
    elif(time == 510) :
        return '2성 LWMMG(MG)가 건조될 예정이야!'
    elif(time == 520) :
        return '3성 브렌(MG)가 건조될 예정이야!'
    elif(time == 540) :
        return '3성 M1919A4(MG)가 건조될 예정이야!'
    elif(time == 550) :
        return '3성 MG42(MG)가 건조될 예정이야!'
    elif(time == 610) :
        return '4성 M60(MG)와 3성 M2HB(MG)가 건조될 예정이야!'
    elif(time == 620) :
        return '4성 Mk48(MG), AEK-999(MG)가 건조될 예정이야!'
    elif(time == 625) :
        return '4성 M1918(MG), 아멜리(MG)가 건조될 예정이야!'
    elif(time == 630) :
        return '4성 PK(MG), MG3(MG)가 건조될 예정이야!'
    elif(time == 635) :
        return '5성 네게브(MG)가 건조될 예정이야!'
    elif(time == 640) :
        return '5성 MG4(MG)가 건조될 예정이야!'
    elif(time == 645) :
        return '5성 MG5(MG)가 건조될 예정이야!'
    elif(time == 650) :
        return '5성 PKP(MG)가 건조될 예정이야!'
    elif(time == 715) :
        return '3성 NS2000(SG)가 건조될 예정이야!'
    elif(time == 720) :
        return '3성 M500(SG)가 건조될 예정이야!'
    elif(time == 725) :
        return '3성 KS-23(SG)가 건조될 예정이야!'
    elif(time == 730) :
        return '3성 RMB-93(SG), M1897(SG)가 건조될 예정이야!'
    elif(time == 740) :
        return '4성 M590(SG), SPAS-12(SG)가 건조될 예정이야!'
    elif(time == 745) :
        return '4성 M37(SG)가 건조될 예정이야!'
    elif(time == 750) :
        return '4성 Super-Shorty(SG)가 건조될 예정이야!'
    elif(time == 755) :
        return '4성 USAS-12(SG)가 건조될 예정이야!'
    elif(time == 800) :
        return '5성 KSG(SG)가 건조될 예정이야!'
    elif(time == 805) :
        return '5성 Saiga-12(SG)가 건조될 예정이야!'
    elif(time == 810) :
        return '5성 S.A.T.8(SG)가 건조될 예정이야!'
    else :
        return '잘못입력한거 같아!'

def kancollemake (target) :
    if(target == 17) :
        return '잠수함 마루유가 건조될 예정이야!'
    elif(target == 18) :
        return '구축함 무츠급이 건조될 예정이야!'
    elif(target == 20) :
        return '구축함 후부키급, 아야나미급, 아카츠키급, 하츠하루급이 건조될 예정이야!'
    elif(target == 22) :
        return '구축함 시라츠유급, 아사시오급 잠수함 이8, 이58, 이168이 건조될 예정이야!'
    elif(target == 24) :
        return '구축함 카게로우급, Z1(비서함이 독일함)급이 건조될 예정이야!'
    elif(target == 30) :
        return '구축함 시마카제가 건조될 예정이야!'
    elif(target == 100) :
        return '중순양함 후루타카급, 아오바급, 경순양함 텐류급, 쿠마급, 나가라급, 센다이급, 아가노, 노시로, 야하기가 건조될 예정이야!'
    elif(target == 110) :
        return '연습순양함 카토리, 중순양함 차라(비서함이 폴라 혹은 리베치오)가 건조될 예정이야!'
    elif(target == 120) :
        return '중순양함 묘코급이 건조될 예정이야!'
    elif(target == 122) :
        return '경순양함 유바리가 건조될 예정이야!'
    elif(target == 125) :
        return '중순양함 타카오급이 건조될 예정이야!'
    elif(target == 130) :
        return '중순양함 모가미, 미쿠마, 스즈야, 쿠마노, 토네급이 건조될 예정이야!'
    elif(target == 200) :
        return '경항공모함 어머늬뮤가 건조될 예정이야!'
    elif(target == 220) :
        return '수상기모함 치토세, 치요다가 건조될 예정이야!'
    elif(target == 230) :
        return '양륙함 아키츠마루가 건조될 예정이야!'
    elif(target == 240) :
        return '경항공모함 쇼호, 즈이호가 건조될 예정이야!'
    elif(target == 250) :
        return '경항공모함 류조가 건조될 예정이야!'
    elif(target == 300) :
        return '경항공모함 히요, 준요가 건조될 예정이야!'
    elif(target == 320) :
        return '잠수함 이401이 건조될 예정이야!'
    elif(target == 400) :
        return '전함 콩고급이 건조될 예정이야!'
    elif(target == 410) :
        return '정규항공모함 소류, 히류가 건조될 예정이야!'
    elif(target == 420) :
        return '전함 후소, 야마시로, 정규항공모함 카가가 건조될 예정이야!'
    elif(target == 430) :
        return '전함 이세, 휴가, 정규항공모함 아카기가 건조될 예정이야!'
    elif(target == 500) :
        return '전함 나가토, 무츠, 비스마르크(비서함이 독일함)가 건조될 예정이야!'
    elif(target == 600) :
        return '정규항공모함 쇼카쿠, 즈이카쿠가 건조될 예정이야!'
    elif(target == 640) :
        return '장갑항공모함 다이호가 건조될 예정이야!'
    elif(target == 800) :
        return '전함 야마토, 무사시가 건조될 예정이야!'
    else :
        return '잘못입력한거 같아!'


