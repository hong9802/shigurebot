import requests
from bs4 import BeautifulSoup
import re

def hansung(url) :
    result = []
    f = open("hansung.txt", 'r')
    line = f.read()
    f.close()
    r = requests.get(url)
    soup = BeautifulSoup(r.text, "html.parser")
    mr = soup.find("table", class_="bbs-list")
    first = mr.find_all("td", class_="subject")
    for i in range(0, 5, 1) :
        second = first[i].find("a")
        strcharge = str(second)
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', strcharge)
        if(str(line[:25]) == cleantext[:25]) :
            return result
        else :
            result.append(cleantext)

def parshingdata(url) :
        result = []
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "html.parser")
        mr = soup.find("table", class_="bbs-list")
        first = mr.find_all("td", class_="subject")
        for i in range(0, 5, 1) :
            second = first[i].find("a")
            strcharge = str(second)
            cleanr = re.compile('<.*?>')
            cleantext = re.sub(cleanr, '', strcharge)
            result.append(cleantext)

        return result

def parshinghan(url) :
        result = []
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "html.parser")
        mr = soup.find("table", class_="bbs-list")
        first = mr.find_all("td", class_="subject")
        for i in range(0, 5, 1):
            second = first[i].find("a")
            strcharge = str(second)
            cleanr = re.compile('<.*?>')
            cleantext = re.sub(cleanr, '', strcharge)
            result.append(cleantext)

        return result

def parshingkan(url) :
        result = []
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "html.parser")
        mr = soup.find_all("div", class_="lst_nm clear ")
        for i in range(0, 5, 1):
            first = mr[i].find("a")
            strcharge = str(first)
            cleanr = re.compile('<.*?>')
            cleantext = re.sub(cleanr, '', strcharge)
            result.append(cleantext)

        return result

def parshingsojo(url) :
        result = []
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "html.parser")
        second = soup.find("ul", class_="clearfix")
        third = second.find_all("li")
        for i in range(0, 5, 1):
            forth = third[i].find("a")
            strcharge = str(forth)
            cleanr = re.compile('<.*?>')
            cleantext = re.sub(cleanr, '', strcharge)
            result.append(cleantext)

        return result

def Parshingdic(url) :
        result = []
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "html.parser")
        mr = soup.find("meta", {"name":"twitter:description"})
        name = soup.find("title")
        strname = str(name)
        strchange = str(mr)
        test = re.sub('<meta content="| " name="twitter:description"/>|" name="twitter:description"/>', '', strchange)
        test2 = re.sub('<title>| – 다음 어학사전</title>', '', strname)
        result.append(test)
        result.append(test2)
        return result

def youtubelistparser(url) :
        playerlist = []
        matching = []
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "html.parser")
        mr = soup.find_all("a", class_=" spf-link playlist-video clearfix yt-uix-sessionlink spf-link ")
        for i in range(0, len(mr), 1) :
            strchange = str(mr[i])
            cleanr = strchange[168:204]
            playerlist.append("http://youtube.com" + cleanr)
            first = mr[i].find("h4")
            strchange2 = str(first)
            test = re.sub('<h4 class="yt-ui-ellipsis yt-ui-ellipsis-2">|          |      </h4>|\n', '', strchange2)
            matching.append(test)

        return (playerlist, matching)