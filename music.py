import discord
import TOKEN
import asyncio
import random
from datetime import datetime

client = discord.Client()

@client.event
async def on_ready():
    print(client.user.name)
    print("=======================")
    channel = discord.Server(id=TOKEN.server1())
    msg = await client.send_message(channel, "g?start")
    await client.delete_message(msg)

@client.event
async def on_message(message) :
    member = ['shigure', 'haruna', 'beruni', 'akatsuki', 'eugen']
    class shibo() :
        def __init__(self, time, member):
            self.time = time
            self.member = member
        async def play(self) :
            voice = await client.join_voice_channel(client.get_channel(TOKEN.server2()))
            player = voice.create_ffmpeg_player('voice/' + self.member + '/' + str(self.time) + '.mp3')
            player.start()
            await client.send_message(discord.Server(id = TOKEN.server3()), "이번 시보 담당자는 " + membershibo(self.member) + "짱이야!")
            await asyncio.sleep(17)
            await voice.disconnect() 

    if(message.content.startswith('g?start')) :
        print('start')
        while True :
            await asyncio.sleep(1)
            if(datetime.now().minute == 0 and datetime.now().second == 0) :
               a = shibo(datetime.now().hour, random.choice(member))
               await a.play()
               del a
               break
        await client.send_message(discord.Server(id = TOKEN.server1()), 'g?start') 

def membershibo(timesignal) :
    if(timesignal == 'shigure') :
        return '시구레'
    elif(timesignal == 'haruna') :
        return '하루나'
    elif(timesignal == 'beruni') :
        return '베르누이'
    elif(timesignal == 'akatsuki') :
        return '아카츠키'
    elif(timesignal == 'eugen') :
        return '프린츠 오이겐'
                    

client.run(TOKEN.TOKEN())
