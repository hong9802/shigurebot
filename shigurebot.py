import discord
import re
import os
import youtube_dl
from datetime import datetime
import socket
import random
import asyncio
import subprocess
import asyncio.coroutines
import iptable
import Parshing
import TOKEN
import recipe


TOKEN = TOKEN.TOKEN()
description = '''시구레봇 입니다.'''
bot = discord.Client()

try:
    print()
except ConnectionRefusedError as e:
    print(e)
    subprocess.check_output('python3 shigurebot.py', shell = True)

except ConnectionResetError as e :
    print(e)
    subprocess.check_output('python3 shigurebot.py', shell = True)

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    f = open('hansung.txt', 'r')
    line = f.read()
    f.close()
    if(line == None) :
        print('초기 data 구축중....')
        result = Parshing.parshingdata('http://hansung.ac.kr/web/www/cmty_01_01')
        f = open("hansung.txt", 'w')
        f.write(result[0])
        f.close()
    else :
        print('정상작동')
        pass

@bot.event
async def on_member_join(member) :
    server = member.server
    fmt = '{0.mention} 야 메우방에 온걸 환영해'
    await bot.send_message(server, fmt.format(member, server))

@bot.event
async def on_message (message) :
    command = ['g?sayd', 'g?day', '서버체크', 'g?사전', '한성공지', 'g?이벤트',
              'g?소전공지', '학사공지', 'g?helping', 'g?tier', 'g?레시피', 'g?소전레시피',
             'g?소전건조', 'g?건조', 'g?play']
    if (message.content.startswith(command[0]) and message.author.name != '시구레봇'):
        msg = await bot.send_message(message.channel, re.sub('[\{\}\[\]\/,;:|\)*`\-_+<>@\#$%&\\\=\(\'\"]', '', message.content[7:]))
        async for msg in bot.logs_from(message.channel):
                if(msg.author.id == message.author.id and msg.content == message.content):
                    await bot.delete_message(msg)
                    break

    elif (message.content.startswith(command[1])):
        nichijo1 = "제독, 불렀어?"
        nichijo2 = "내게 흥미가 있어? ……괜찮아. 뭐든 물어봐."
        nichijo3 = "아쉽게 됐네."
        nichijo4 = "제독, 편지가 와 있어."
        nichijo5 = "나는 아직, 여기에 있어도 괜찮은 걸까……?"
        nichijo6 = "비는 언젠가 그쳐."
        voice = [nichijo1, nichijo2, nichijo3, nichijo4, nichijo5, nichijo6]
        await bot.send_message(message.channel, random.choice(voice))

    elif (message.content.startswith(command[2]) and message.author.name == 'ホンダァルゥ') :
        responce = os.system(iptable.home())
        if responce == 0 :
            await bot.send_message(message.channel, "NAS서버는 살아있어!")
        else :
            await bot.send_message(message.channel, "NAS서버가 죽은거 같아...")
        responce2 = os.system(iptable.home2())
        if responce2 == 0 :
            await bot.send_message(message.channel, "iptime DNS서버는 살아있어!")
        else :
            await bot.send_message(message.channel, "iptime DNS서버가 죽은거 같아...")
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((iptable.hong(), 4202))
        if result == 0 :
            await bot.send_message(message.channel, "데스크탑 켜져 있어!")
        else :
            await bot.send_message(message.channel, "데스크탑이 죽은거 같아...")
            try :
                print()
            except RuntimeError as e :
                print(e)

    elif (message.content.startswith(command[3])) :
        msg = message.content[5:]
        url = 'http://alldic.daum.net/search.do?q=' + msg
        result = Parshing.Parshingdic(url)
        if (result[0] == "") :
            await bot.send_message(message.channel, msg + '라는 단어는 없는 거 같아...')
        else :
            await bot.send_message(message.channel, result[1] + ' 라는 뜻은...')
            await bot.send_message(message.channel, result[0] + ' 라는 뜻이야!')

    elif (message.content.startswith('g?start')) :
        print('check... hansung...')
        result = Parshing.hansung('http://hansung.ac.kr/web/www/cmty_01_01')
        string = ''
        if(result == None) :
            print("새로운 공지 없음.")
        elif (len(result) == 0) :
            print('none') 
        else :
            for i in range(0, len(result), 1) :
                a = str(result[i])
                b = a + '\n'
                string+=b

            embed = discord.Embed(
                title = "新しい 한성공지가 떳어!!",
                color = 0x2E9AFE,
                description = string
                )
            embed.set_author(
                name = '한성대학교',
                icon_url = "http://hansung.ac.kr/html/themes/www/img/custom/1_01_05_01_obj_2.gif",
                url = "http://hansung.ac.kr/web/www/cmty_01_01"
                )
            f = open('hansung.txt', 'w')
            print(result[0])
            f.write(result[0])
            f.close()
            await bot.send_message(discord.Server(id = TOKEN.hansung()), embed = embed)

    elif (message.content.startswith(command[4])):
        print('한성공지')
        result = Parshing.parshingdata('http://hansung.ac.kr/web/www/cmty_01_01')
        embed = discord.Embed(
                title="한성대학교 한성공지",
                color = 0x2E9AFE,
                description = result[0] + "\n"
                              + result[1] + "\n"
                              + result[2] + "\n"
                              + result[3] + "\n"
                              + result[4] + "\n"
            )
        embed.set_author(
            name = "한성대학교",
            icon_url = "http://hansung.ac.kr/html/themes/www/img/custom/1_01_05_01_obj_2.gif",
            url = "http://hansung.ac.kr/web/www/cmty_01_01"
            )
        await bot.send_message(message.channel, embed = embed)

    elif (message.content.startswith(command[5]) and message.author.id != '시구레봇'):
        result   = Parshing.parshingkan("http://wgforum.kr/kancolle_news")
        embed = discord.Embed(
                title = "칸코레 이벤트 공지",
                color = 0x00ff00,
                description = result[0] + "\n"
                              + result[1] + "\n"
                              + result[2] + "\n"
                              + result[3] + "\n"
                              + result[4] + "\n"
            )
        embed.set_author(
            name = "칸코레!",
            icon_url = "https://vignette.wikia.nocookie.net/kancolle/images/0/09/Kancolle_logo_v2.png/revision/latest?cb=20141209055745",
            url = "http://wgforum.kr/kancolle_news"
            )
        await bot.send_message(message.channel, embed = embed)

    elif (message.content.startswith(command[6]) and message.author.id != '시구레봇'):
        result = Parshing.parshingsojo("http://www.girlsfrontline.co.kr/archives/category/news")
        embed = discord.Embed(
                titile = "소녀전선 공지!",
                color = 0x00ff00,
                description = result[0] + "\n"
                              + result[1] + "\n"
                              + result[2] + "\n"
                              + result[3] + "\n"
                              + result[4] + "\n"
            )
        embed.set_author(
            name = "소녀전선!",
            icon_url = "http://www.girlsfrontline.co.kr/wp-content/themes/twentysixteen/images/home/logo.png?v22",
            url = "http://www.girlsfrontline.co.kr/archives/category/news"
            )
        await bot.send_message(message.channel, embed = embed)


    elif (message.content.startswith(command[7]) and message.author.id != '시구레봇'):
        result = Parshing.parshinghan('http://hansung.ac.kr/web/www/cmty_01_03')
        embed = discord.Embed(
                title = "한성대학교 학사공지!",
                color = 0x2E9AFE,
                description = result[0] + "\n"
                              + result[1] + "\n"
                              + result[2] + "\n"
                              + result[3] + "\n"
                              + result[4] + "\n"
            )
        embed.set_author(
            name = "한성대학교",
            icon_url = "http://hansung.ac.kr/html/themes/www/img/custom/1_01_05_01_obj_2.gif",
            url = "http://hansung.ac.kr/web/www/cmty_01_03"
            )
        await bot.send_message(message.channel, embed = embed)

    elif (message.content.startswith(command[8]) and message.author.name != '시구레봇'):
        await bot.send_message(message.channel, "```\n"
                  "한성공지 = 한성대학교 한성공지를 알려줘!\n"
                  "sayd 메시지 = 내가 대신 말을 전해줄게!\n"
                  "학사공지 = 한성대학교 학사공지를 전해줄게!\n"
                  "tier 최소 최대 = 탱크 티어 결정해줄게!\n"
                  "레시피 Code = Code는 (구축함, 경순양함, 중순양함, 전함datetime.now().today, 항공모함, 잠수함) 이야!\n"
                  "소전레시피 Code = Code는 (HG, SMG, AR, RF, SPAR, MG, SG, 네게브, 범용, 중형범용, 검열해제) 이야!\n"
                  "소전건조 숫자 = 소녀전선에서 나오는 애들을 알려줄게!(아래와 동일!)\n"
                  "건조 숫자 = 칸코레에서 누가 나올지 알려줄게!(만약 8:00이면 800, 1:25이면 125)\n"
                  "소전공지 = 소녀전선 이벤트 정보를 알려줄게!\n"
                  "이벤트 = 칸코레 이벤트 정보를 알려줄게!\n"
                  "숨겨진 커멘드가 있을지도?\n"
                               "```")

    elif (message.content.startswith(command[9]) and message.author.name != '시구레봇'):
        msg = message.content[7:]
        msg.split()
        tmax = int(msg[2:4]) + 1
        tmin = int(msg[0])
        if(tmin > 0 and tmin < 11 and tmax < 12 and tmax > 0) :
            await bot.send_message(message.channel, str(random.randrange(tmin, tmax)) + " 티어가 좋아보여!")
        else :
            await bot.send_message(message.channel, '조건은 <최소>티어 <최대>티어 순입니다. 다시 입력 부탁드려요!')

    elif (message.content.startswith('<@256334494716395520>') and message.author.name != '시구레봇'):
        today = datetime.now().day
        today2 = datetime.now().month
        if(today2 == 1 and today > 17 and today < 25):
            await bot.send_message(message.channel, "우리 개발자님은 일본으로 유배당했어!")

    elif(message.content.startswith(command[10]) and message.author.name != '시구레봇') :
        recype = message.content[6:]
        await bot.send_message(message.channel, '연료 탄약 강재 보크사이트 순이야!')
        await bot.send_message(message.channel, recipe.kancolle(recype))

    elif(message.content.startswith(command[11]) and message.author.name != '시구레봇') :
        recype = message.content[8:]
        await bot.send_message(message.channel, '인력 탄약 식량 부품 순이야!')
        await bot.send_message(message.channel, recipe.girlsfront(recype))

    elif(message.content.startswith(command[12]) and message.author.name != '시구레봇'):
        time2 = message.content[7:]
        time = int(time2)
        await bot.send_message(message.channel, recipe.girlsmake(time))

    elif (message.content.startswith(command[13]) and message.author.name != '시구레봇'):
        maintarget = message.content[5:]
        target = int(maintarget)
        await bot.send_message(message.channel, recipe.kancollemake(target))

    elif (message.content.startswith('g?mes') and message.author.name != '시구레봇' ) :
        await bot.send_message(discord.Server(id = "332873764784308225"), 'test')

    elif (message.content.startswith(command[14]) and message.author.name != '시구레봇') :
        channel = message.author.voice.voice_channel
        url = message.content[7:]
        playlist = Parshing.youtubelistparser(url)
        voice = await bot.join_voice_channel(channel)
        for i in range(0, len(playlist[0]), 1) :
            player = await voice.create_ytdl_player(playlist[0][i])
            await bot.send_message(message.channel, 'playing ' + playlist[1][i])
            player.start()
            await asyncio.sleep(player.duration)
        await bot.send_message(message.channel, 'finish')

bot.run(TOKEN)
